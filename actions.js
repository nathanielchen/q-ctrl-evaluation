export const actionTypes = {
  FAILURE: 'FAILURE',
  LOAD_EVALUATIONS: 'LOAD_EVALUATIONS',
  LOAD_EVALUATIONS_SUCCESS: 'LOAD_EVALUATIONS_SUCCESS',

  SELECTED_EVALUATION: 'SELECTED_EVALUATION',
  LOAD_SELECTED_EVALUATION: 'LOAD_SELECTED_EVALUATION',
  LOAD_SELECTED_EVALUATION_SUCCESS: 'LOAD_SELECTED_EVALUATION_SUCCESS',
}

export function failure(error) {
  return {
    type: actionTypes.FAILURE,
    error,
  }
}

export function loadEvaluations() {
  return { type: actionTypes.LOAD_EVALUATIONS }
}

export function loadEvaluationsSuccess(data) {
  return {
    type: actionTypes.LOAD_EVALUATIONS_SUCCESS,
    data,
  }
}

export function slectedEvaluation(itemId) {
  return { type: actionTypes.LOAD_SELECTED_EVALUATION, id: itemId }
}

export function loadSelectedEvaluationsSuccess(data) {
  return {
    type: actionTypes.LOAD_SELECTED_EVALUATION_SUCCESS,
    data,
  }
}
