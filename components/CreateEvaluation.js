import Link from 'next/link'
import { connect } from 'react-redux'
import ErrorMessage from './ui/ErrorMessage'
import Header from './ui/Header'
import Wrapper from './ui/Wrapper'
import Container from './ui/Container'

function CreateEvaluation({ error, evaluationsData, newEvaluation }) {
  return (
    <div>
      <Wrapper>
        <h1>CREATE A NEW</h1>
        <h1>EVALUATION</h1>

        <label>TitleM</label>
        <input placeholder="New Evaluation" />
        <span> Enter a title</span>

        <label>Maximum Rabi Rate</label>
        <input placeholder="0.0001" />
        <span>Enter maximum rabi rate</span>

        <label>Polar Angle</label>
        <input placeholder="10" />
        <span>Enter polar angle</span>

        <button>Create Evaluation</button>
        <p>
          There was an error: Ensure that there are no more than 1 digits before
          the decimal point.
        </p>
        {newEvaluation && (
          <pre>
            <code>{JSON.stringify(newEvaluation, null, 2)}</code>
          </pre>
        )}
        <ErrorMessage error={error} />
      </Wrapper>
    </div>
  )
}

export default connect(state => state)(CreateEvaluation)
