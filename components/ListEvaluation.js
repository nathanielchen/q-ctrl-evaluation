import React, { Component } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import ErrorMessage from './ui/ErrorMessage'
import Header from './ui/Header'
import Wrapper from './ui/Wrapper'
import Container from './ui/Container'
import ImageButton from './ui/ImageButton'
import Table from './ui/Table'
import CreateEvaluation from './CreateEvaluation'

import Modal from './ui/Modal'

class ListEvaluation extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowModal: false,
    }
  }

  closeModalHandler = () => {
    this.setState({ isShowModal: false })
  }
  showModalHandler = () => {
    this.setState({ isShowModal: true })
  }

  render() {
    const { error, evaluationsData } = this.props
    let evaluationList
    if (evaluationsData) {
      evaluationList = <Table evaluationsData={evaluationsData} />
    } else {
      evaluationList = <h4>Loading Evaluations Data...</h4>
    }
    return (
      <Wrapper>
        <Modal
          show={this.state.isShowModal}
          modalClosed={this.closeModalHandler}
        >
          <CreateEvaluation />
        </Modal>

        <Header />
        <Container mainBody>
          <h1 className="row middle">
            List Evaluation
            <div onClick={this.showModalHandler}>
              <ImageButton icon="add.svg" />
            </div>
          </h1>
          {evaluationList}
        </Container>
        <ErrorMessage error={error} />
      </Wrapper>
    )
  }
}
export default connect(state => state)(ListEvaluation)
