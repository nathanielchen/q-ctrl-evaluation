import Link from 'next/link'
import { connect } from 'react-redux'
import ErrorMessage from './ui/ErrorMessage'
import Header from './ui/Header'
import Wrapper from './ui/Wrapper'
import Container from './ui/Container'
import Plot from 'react-plotly.js'

function ViewEvaluation({ error, selectedEvaluation }) {
  console.log('selectedEvaluation', selectedEvaluation)
  return (
    <Wrapper>
      <Header />

      <Wrapper>
        {selectedEvaluation && (
          <h1 className="left">
            {`${selectedEvaluation.data.type} / ${
              selectedEvaluation.data.attributes.name
            }`}
          </h1>
        )}
        {selectedEvaluation ? (
          selectedEvaluation.included.map((pulse, i) => (
            <Plot
              key={i}
              data={[
                {
                  x: pulse.attributes.times,
                  y: pulse.attributes.x_amplitudes,
                  name: 'X',
                  type: 'scatter',
                },
                {
                  x: pulse.attributes.times,
                  y: pulse.attributes.y_amplitudes,
                  name: 'Y',
                  type: 'scatter',
                },
                {
                  x: pulse.attributes.times,
                  y: pulse.attributes.z_amplitudes,
                  name: 'Z',
                  type: 'scatter',
                },
              ]}
              layout={{
                width: 400,
                height: 300,
                title: `${pulse.type}`,
              }}
            />
          ))
        ) : (
          <h4>Loading Evaluations Pulse...</h4>
        )}
        <ErrorMessage error={error} />
      </Wrapper>

      <style jsx global>{`
        .js-plotly-plot .plotly div {
          margin: 10px;
        }
      `}</style>
    </Wrapper>
  )
}

export default connect(state => state)(ViewEvaluation)
