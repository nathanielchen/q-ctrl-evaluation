import React, { Component } from 'react'

class Container extends Component {
  render() {
    return (
      <div className={`container${this.props.mainBody ? ' mainBody' : ''}`}>
        {this.props.children}
        <style jsx>{`
          .container {
            text-align: left;
            max-width: 1050px;
            margin: auto;
          }
          .mainBody {
            padding: 1rem;
          }
        `}</style>
      </div>
    )
  }
}

export default Container
