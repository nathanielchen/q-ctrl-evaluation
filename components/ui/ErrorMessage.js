import React from 'react'

const Error = props =>
  props.error && (
    <p className="error_message">
      Error: {props.error.message}
      <style jsx>{`
        .error_message {
          color: red;
        }
      `}</style>
    </p>
  )

export default Error
