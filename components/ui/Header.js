import { Q_THEME } from '../../shared/constants'
import Container from './Container'

import Router from 'next/router'

function Header({ title }) {
  return (
    <div className="header">
      <Container>
        <img
          src={`/static/logo.svg`}
          alt="Q-Ctrl logo"
          onClick={() => {
            Router.push('/')
          }}
        />
      </Container>
      <style jsx>{`
        .header {
          margin: auto;
          padding: 0.4375rem 1.375rem;
          background-color: ${Q_THEME.HEADER_COLOR};
        }
      `}</style>
    </div>
  )
}

export default Header
