import React, { Component } from 'react'

class ImageButton extends Component {
  render() {
    const { icon } = this.props
    return (
      <div className="image_button ">
        <img src={`/static/${icon}`} alt={icon} />
        <style jsx>{`
          .image_button {
            cursor: pointer;
            padding: 0.2rem;
          }
        `}</style>
      </div>
    )
  }
}

export default ImageButton
