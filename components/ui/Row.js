import React, { Component } from 'react'
import { Q_THEME } from '../../shared/constants'
import ImageButton from './ImageButton'
import Router from 'next/router'
import { connect } from 'react-redux'
import { slectedEvaluation } from '../../actions'

class Row extends Component {
  slectedEvaluation = itemId => {
    this.props.dispatch(slectedEvaluation(itemId))
  }

  render() {
    const { type, attributes, id } = this.props

    return (
      <div className="row table_row">
        <div className="table-item item-name">{attributes.name}</div>
        <div className="table-item">
          <div className="type_wrapper">{type}</div>
        </div>
        <div className="table-item">{attributes.polar_angle}</div>
        <div className="table-item big_item">
          {attributes.maximum_rabi_rate}
        </div>
        <div
          className="table-item late_item"
          onClick={() => {
            this.slectedEvaluation(id)
            Router.push('/view_evaluation')
          }}
        >
          <ImageButton icon="chevron_right.svg" />
        </div>
        <style jsx>{`
          .table_row {
            font-size: 12px;
            color: ${Q_THEME.TABBLE_TEXT_COLOR};
            border-top: ${`1px solid ${Q_THEME.TABLE_BORDER_COLOR}`};
            background-color: white;
          }
          .item-name {
            font-size: 14px;
          }
          .table-item {
            flex: 1;
            padding: 28px 28px;
          }
          .type_wrapper {
            background: ${Q_THEME.TYPE_WRAPPER_COLOR};

            color: #fff;
            text-align: center;
            padding: 2px 10px;
            border-radius: 3px;
          }
          .big_item {
            flex: 3;
          }
          .late_item {
            text-align: end;
          }
        `}</style>
      </div>
    )
  }
}

export default connect()(Row)
