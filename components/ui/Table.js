import React, { Component } from 'react'
import Row from './Row'

import { Q_THEME } from '../../shared/constants'

class Table extends React.Component {
  render() {
    const { evaluationsData } = this.props
    let rows
    if (evaluationsData) {
      rows = evaluationsData.data.map(evaluation => (
        <Row key={evaluation.id} {...evaluation} />
      ))
    } else {
      rows = null
    }
    return (
      <div className="table">
        <div className="header row">
          <div className="uppercase header_item">Title</div>
          <div className="uppercase header_item">Type</div>
          <div className="uppercase header_item">Polar Angle</div>
          <div className="uppercase header_item big_item">Max Rabi Rate</div>
          <div className="uppercase header_item" />
        </div>
        <div className="body">{rows}</div>

        <style jsx>{`
          .table {
            margin: 1em;
            display: flex;
            flex-direction: column;

            .header {
              font-size: 11px;
              color: ${Q_THEME.GREY};
              border-top: ${`1px solid ${Q_THEME.TABLE_BORDER_COLOR}`};
              background-color: ${Q_THEME.BACKGROUND_COLOR};
            }
            .header_item {
              flex: 1;
              padding: 16px 28px;
            }
            .big_item {
              flex: 3;
            }
          }
        `}</style>
      </div>
    )
  }
}

export default Table
