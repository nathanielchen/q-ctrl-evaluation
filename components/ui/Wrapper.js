import React, { Component } from 'react'
import { Q_THEME } from '../../shared/constants'

class Wrapper extends Component {
  render() {
    return (
      <div className="wrapper center">
        {this.props.children}
        <style jsx>{`
          .wrapper {
            background-color: ${Q_THEME.BACKGROUND_COLOR};
            width: 100%;
            height: 100%;
          }
        `}</style>
      </div>
    )
  }
}

export default Wrapper
