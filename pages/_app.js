import App, { Container } from 'next/app'
import React from 'react'
import { Provider } from 'react-redux'
import withRedux from 'next-redux-wrapper'
import withReduxSaga from 'next-redux-saga'
import { Q_THEME } from '../shared/constants'

import createStore from '../store'

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx })
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps, store } = this.props
    return (
      <Container>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
        <style jsx global>{`
          html,
          body {
            margin: 0px;
            font-family: ${Q_THEME.FONTFAMILY};
            min-height: 100%;
          }
          #__next {
            min-height: 100%;
          }
          h1 {
            font-weight: 500;
            font-size: 18px;
          }
          .flex {
            display: flex;
          }
          .row {
            box-sizing: border-box;
            display: flex;
            flex: 0 1 auto;
            flex-direction: row;
            flex-wrap: wrap;
          }
          .between {
            justify-content: space-between;
          }
          .middle {
            align-items: center;
          }
          .center {
            justify-content: center;
            text-align: center;
          }
          .uppercase {
            text-transform: uppercase;
          }
          .left {
            padding: 20px;
            text-align: left;
          }
        `}</style>
      </Container>
    )
  }
}

export default withRedux(createStore)(withReduxSaga({ async: true })(MyApp))
