import React from 'react'
import { connect } from 'react-redux'

import CreateEvaluation from '../components/CreateEvaluation'

class CreateEvaluationPage extends React.Component {
  static async getInitialProps(props) {
    const { store, isServer } = props.ctx

    return { isServer }
  }

  render() {
    return <CreateEvaluation />
  }
}

export default connect()(CreateEvaluationPage)
