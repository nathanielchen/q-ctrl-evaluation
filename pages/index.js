import React from 'react'
import { connect } from 'react-redux'

import { loadEvaluations } from '../actions'
import ListEvaluation from '../components/ListEvaluation'

class Index extends React.Component {
  static async getInitialProps(props) {
    const { store, isServer } = props.ctx

    if (!store.getState().placeholderData) {
      store.dispatch(loadEvaluations())
    }

    return { isServer }
  }

  render() {
    return <ListEvaluation />
  }
}

export default connect()(Index)
