import React from 'react'
import { connect } from 'react-redux'

import ViewEvaluation from '../components/ViewEvaluation'

class ViewEvaluationPage extends React.Component {
  static async getInitialProps(props) {
    const { store, isServer } = props.ctx

    return { isServer }
  }

  render() {
    return <ViewEvaluation />
  }
}

export default connect()(ViewEvaluationPage)
