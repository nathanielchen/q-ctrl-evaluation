import { actionTypes } from './actions'

export const initialState = {
  evaluationsData: null,
  selectedEvaluation: null,
  newEvaluation: null,
  error: false,
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FAILURE:
      return {
        ...state,
        ...{ error: action.error },
      }

    case actionTypes.LOAD_EVALUATIONS_SUCCESS:
      return {
        ...state,
        ...{ evaluationsData: action.data },
      }

    case actionTypes.LOAD_SELECTED_EVALUATION_SUCCESS:
      return {
        ...state,
        ...{ selectedEvaluation: action.data },
      }

    default:
      return state
  }
}

export default reducer
