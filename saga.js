/* global fetch */

import { all, call, put, take, takeLatest } from 'redux-saga/effects'
import es6promise from 'es6-promise'
import 'isomorphic-unfetch'

import {
  actionTypes,
  failure,
  loadEvaluationsSuccess,
  loadSelectedEvaluationsSuccess,
} from './actions'

es6promise.polyfill()

function* loadEvaluationsSaga() {
  try {
    const res = yield fetch('http://q-ctrl-api.appspot.com/evaluations/')
    const data = yield res.json()
    yield put(loadEvaluationsSuccess(data))
  } catch (err) {
    yield put(failure(err))
  }
}

function* loadSelectedEvaluationsSaga(item) {
  console.log('loadSelectedEvaluationsSaga', item)

  try {
    const res = yield fetch(
      `http://q-ctrl-api.appspot.com/evaluations/${item.id}?include=pulses`
    )
    const data = yield res.json()
    yield put(loadSelectedEvaluationsSuccess(data))
  } catch (err) {
    yield put(failure(err))
  }
}

function* rootSaga() {
  yield all([takeLatest(actionTypes.LOAD_EVALUATIONS, loadEvaluationsSaga)])
  yield all([
    takeLatest(
      actionTypes.LOAD_SELECTED_EVALUATION,
      loadSelectedEvaluationsSaga
    ),
  ])
}

export default rootSaga
